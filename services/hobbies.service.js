const hobbiesModel = require('../schemas/hobbies.schema')




const getHobbies = async () => {
    return await hobbiesModel.module.find().limit(20)
}
exports.module = {
    getHobbies
}