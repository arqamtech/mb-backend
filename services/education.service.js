const educationModel = require('../schemas/education.schema')


const getEducation = async () => {
    return await educationModel.module.find().limit(20)
}
exports.module = {
    getEducation
}