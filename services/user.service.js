const userModel = require('../schemas/user.schema')




const saveUser = async () => {
    const savedUser = await userModel.module.create({
        name: "Arqam",
        email: "techarqam@gmail.com",
        address: "Sample Add"
    })
    return savedUser
}
const getUsers = async () => {
    const users = await userModel.module.find()
    return users
}
exports.module = {
    saveUser,
    getUsers
}