const Hobbieservice = require('../services/hobbies.service')

const getHobbies = async (req, res) => {
    try {
        const Hobbies = await Hobbieservice.module.getHobbies()
        res.status(200).send({ status: 'success', data: Hobbies || [] })
    } catch (error) {
        console.error('error', error)
        res.status(500).send({ status: 'error', data: null, error })
    }
}

exports.module = {
    getHobbies,
}