const userController = require('./users.controller')
const hobbiesController = require('./hobbies.controller')
const educationController = require('./education.controller')

exports.module = {
    userController,
    hobbiesController,
    educationController
}
