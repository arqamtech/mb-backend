const userService = require('../services/user.service')

const getUsers = async (req, res) => {
    try {
        const users = await userService.module.getUsers()
        res.status(200).send({ status: 'success', data: users || [] })
    } catch (error) {
        res.status(500).send({ status: 'error', data: null, error })
    }
}

const saveUser = async (req, res) => {
    try {
        const savedUser = await userService.module.saveUser()
        res.status(200).send({ status: 'success', data: savedUser || {} })
    } catch (error) {
        res.status(500).send({ status: 'error', data: null, error })
    }
}


exports.module = {
    getUsers,
    saveUser
}