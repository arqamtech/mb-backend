const Educationervice = require('../services/education.service')

const getEducation = async (req, res) => {
    try {
        const Education = await Educationervice.module.getEducation()
        res.status(200).send({ status: 'success', data: Education || [] })
    } catch (error) {
        console.error('error', error)
        res.status(500).send({ status: 'error', data: null, error })
    }
}

exports.module = {
    getEducation,
}