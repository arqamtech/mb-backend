const express = require("express");
const router = express.Router();
const { module: {
  userController,
  hobbiesController,
  educationController
} } = require('../controllers/index')


/* GET home page. */
router.get("/", (req, res) => {
  res.render("index", { title: "Express" });
});
router.get("/test", (req, res) => {
  res.send("hello");
});

// users
router.get('/users', userController.module.getUsers)
router.post('/users', userController.module.saveUser)
// Hobbies
router.get('/hobbies', hobbiesController.module.getHobbies)
// Education
router.get('/education', educationController.module.getEducation)

module.exports = router;
