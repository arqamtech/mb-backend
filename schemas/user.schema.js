const mongoose = require('mongoose')
const { ObjectId } = require('mongoose/lib/drivers/node-mongodb-native')
const { Schema } = mongoose


const userSchema = new Schema({
    name: String,
    email: String,
    address: String,
    Hobbies: [ObjectId],
    education: ObjectId
})
const User = mongoose.model('User', userSchema)
exports.module = User
